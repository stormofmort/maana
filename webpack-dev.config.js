const merge = require('webpack-merge');
const baseConfig = require('./webpack-common.config.js');

module.exports = merge(baseConfig,{
  mode: 'development',
  watch: true,
  devtool: 'inline-source-map',
  module: {
    rules: [
      
      // this will apply to both plain `.css` files
      // AND `<style>` blocks in `.vue` files
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader'
        ]
      }
    ]
  },
  resolve: {
    alias: {
      vue: 'vue/dist/vue.js'
    }
  }
});

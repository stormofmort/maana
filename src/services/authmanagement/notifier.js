const path = require('path');
const pug = require('pug');
const agenda = require('../../jobs/agenda')

module.exports = function(app) {

  function getLink(type, hash) {
    const url = process.env.PROTOCOL+ '://'+ process.env.OUTSIDE_HOST+ '/#/' + type + '/' + hash;
    return url;
  }
  
  return {
    notifier: function(type, user) {
      let tokenLink, email, template, loginLink;
      switch (type) {
      case 'resendVerifySignup': //sending the user the verification email
        tokenLink = getLink('verify', user.verifyToken);
        return agenda.now('registration email', {user, tokenLink})
        
  
      case 'verifySignup': // confirming verification
        loginLink = process.env.PROTOCOL+ '://'+ process.env.OUTSIDE_HOST +'/#/login';
        return agenda.now('email verified', {user, loginLink})
  
      case 'sendResetPwd':
        tokenLink = getLink('verifyreset', user.resetToken); 
        return agenda.now('reset password', {user, tokenLink})

      case 'resetPwd':
        loginLink = process.env.PROTOCOL+ '://'+ process.env.OUTSIDE_HOST +'/#/login'; 
        return agenda.now('password verified', {user, loginLink})

      case 'passwordChange':
        email = {};
        //return sendEmail(email);
  
      case 'identityChange':
        tokenLink = getLink('verifyChanges', user.verifyToken);
        email = {};
        //return sendEmail(email);
  
      default:
        break;
      }
    }
  };
};

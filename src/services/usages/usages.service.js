// Initializes the `usages` service on path `/usages`
const createService = require('feathers-mongoose');
const createModel = require('../../models/usages.model');
const hooks = require('./usages.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/usages', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('usages');

  service.hooks(hooks);
};

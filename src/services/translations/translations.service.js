// Initializes the `translations` service on path `/translations`
const createService = require('feathers-mongoose');
const createModel = require('../../models/translations.model');
const hooks = require('./translations.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/translations', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('translations');

  service.hooks(hooks);
};

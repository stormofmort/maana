// Initializes the `tenses` service on path `/tenses`
const createService = require('feathers-mongoose');
const createModel = require('../../models/tenses.model');
const hooks = require('./tenses.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/tenses', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('tenses');

  service.hooks(hooks);
};

// Initializes the `examples` service on path `/examples`
const createService = require('feathers-mongoose');
const createModel = require('../../models/examples.model');
const hooks = require('./examples.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/examples', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('examples');

  service.hooks(hooks);
};

// Initializes the `dialects` service on path `/dialects`
const createService = require('feathers-mongoose');
const createModel = require('../../models/dialects.model');
const hooks = require('./dialects.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/dialects', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('dialects');

  service.hooks(hooks);
};

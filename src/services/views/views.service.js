// Initializes the `views` service on path `/views`
const createService = require('feathers-mongoose');
const createModel = require('../../models/views.model');
const hooks = require('./views.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/views', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('views');

  service.hooks(hooks);
};

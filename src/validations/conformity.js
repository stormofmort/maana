const onlydhivehi = {
  validator: 'matches',
  arguments: /^[\u0780-\u07B1]+$/,
  message: 'Only dhivehi letters can be used'
};

const letterwithfili = {
  validator: 'matches',
  arguments: /^(([\u0780-\u07A5][\u07A6-\u07B0])|\u0782)+$/,
  message: 'Letter not followed by a fili except husnoonu'
};

const firstlettersukun = {
  validator: 'matches',
  arguments: /^[\u0780-\u07A5][\u07A6-\u07AF]/,
  message: 'First letter must have fili and not sukun'
};

const wronglettersukun = {
  validator: 'matches',
  arguments: /[\u0781\u0782\u0787\u078C\u0790]\u07B0|^(([\u0780-\u07A5][\u07A6-\u07AF])|\u0782)+$/,
  message: 'Wrong letter with sukun'
};

const sukunexists = {
  validator: 'matches',
  arguments: /^\u07B0+$/,
  message: 'Sukun exists'
};

module.exports={
  onlydhivehi, letterwithfili, firstlettersukun, wronglettersukun, sukunexists
};
const agenda = require('./agenda')
const pug = require('pug');
const path = require('path');

module.exports = function(app) {
  function sendEmail(email, done, job) {
    return app.service('mailer').create(email).then(function (result) {
      // eslint-disable-next-line no-console
      console.log('Sent email from worker', result);
      done()
    }).catch(err => {
      // eslint-disable-next-line no-console
      console.log('Error sending email', err);
      job.repeatAt('15 minutes from now')
      job.save();
      done(err)
    });
  }

  agenda.define('registration email',{concurrency:10, priority:'high'}, (job, done) => {
    var template = pug.compileFile(path.join(app.get('src'), 'email-templates', 'account', 'verify.pug'));
    var email = {
      from: process.env.SMTP_USER,
      to: job.attrs.data.user.email,
      subject: 'Verify Signup',
      html: template({tokenLink: job.attrs.data.tokenLink, user:job.attrs.data.user, unsubscribe:false})
    };
    sendEmail(email, done, job);
  });

  agenda.define('email verified',{concurrency:10, priority:'high'}, (job, done) => {
    var template = pug.compileFile(path.join(app.get('src'), 'email-templates', 'account', 'verified.pug'));
    var email = {
      from: process.env.SMTP_USER,
      to: job.attrs.data.user.email,
      subject: 'Signup verified',
      html: template({loginLink: job.attrs.data.loginLink, user:job.attrs.data.user, unsubscribe:false})
    };
    sendEmail(email, done, job)
  });

  agenda.define('reset password', {concurrency:10, priority: 'high'}, (job, done)=>{
    var template = pug.compileFile(path.join(app.get('src'), 'email-templates', 'account', 'reset.pug'));
    email = {
      from: process.env.SMTP_USER,
      to: job.attrs.data.user.email,
      subject: 'Reset Password',
      html: template({tokenLink: job.attrs.data.tokenLink, user: job.attrs.data.user, unsubscribe:false})
    };
    sendEmail(email, done, job)
  })

  agenda.define('password verified', {concurrency:10, priority: 'high'}, (job, done)=>{
    var template = pug.compileFile(path.join(app.get('src'), 'email-templates', 'account', 'reset-verified.pug'));
    email = {
      from: process.env.SMTP_USER,
      to: job.attrs.data.user.email,
      subject: 'Password reset',
      html: template({loginLink:job.attrs.data.loginLink, user:job.attrs.data.user, unsubscribe:false})
    };
    sendEmail(email, done, job)
  })
}

const Agenda = require('agenda');

// const mongooseClient = app.get('mongooseClient');
// const dbConnection = mongooseClient.connection;

// const agenda = new Agenda({mongo: dbConnection});

const connectionOpts = {db: {address: process.env.AGENDA_DB_URL, collection: 'agendaJobs', options:{useNewUrlParser: true}}};

const agenda = new Agenda(connectionOpts);

module.exports = agenda

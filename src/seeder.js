const seeder = require('feathers-seeder');
const userSeeder = require('../seeds/users');
const wordSeeder = require('../seeds/words')();

async function test() {
  return {
    seederOptions: seeder({
      services: [
        await userSeeder,
        await wordSeeder
      ]
    })
  };
}

module.exports = test();

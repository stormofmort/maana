const mongoose = require('mongoose');

module.exports = function (app) {

  mongoose.connect(
    app.get('appdb'),
    { useCreateIndex: true, useNewUrlParser: true }
  );
  mongoose.Promise = global.Promise;

  app.set('mongooseClient', mongoose);
};

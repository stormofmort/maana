// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  return async context => {
    const { data } = context;
    const user = context.params.user;
    
    const text = data.text.substring(0,20);
    const published = data.published;

    context.data = {
      text,
      userId: user._id,
      published
    };

    return context;
  };
};

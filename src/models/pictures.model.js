// pictures-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const pictures = new Schema({
    filename: { type: String, required: true },
    fileext: { type: String, required: true },
    voteId: [{type: Schema.Types.ObjectId}],
    commentId: [{type: Schema.Types.ObjectId}],
  }, {
    timestamps: true
  });

  return mongooseClient.model('pictures', pictures);
};

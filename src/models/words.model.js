const {onlydhivehi, letterwithfili, firstlettersukun, wronglettersukun} = require('../validations/conformity');
const validate = require('mongoose-validator');
// words-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.

module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const usages = new Schema({
    _id: {type: Schema.Types.ObjectId},
    usetype: {type:String}
  })
  const words = new Schema({
    text: { 
      type: String, 
      required: true, 
      validate: [validate(onlydhivehi), validate(letterwithfili), validate(firstlettersukun), validate(wronglettersukun)], 
      unique: true 
    },
    published: {type: Boolean, default:false},
    is_deleted: {type: Boolean, default:false},
    delete_date: {type: Date},
    userId: {type: Schema.Types.ObjectId, required: true },
    usages: [usages],
    voteId: [{type: Schema.Types.ObjectId}],
    commentId: [{type: Schema.Types.ObjectId}],
    viewId: [{type: Schema.Types.ObjectId}],
  }, {
    timestamps: true
  });
  return mongooseClient.model('words', words);
};

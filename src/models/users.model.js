// users-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const users = new mongooseClient.Schema({
  
    username: {type: String, unique:true, requied:true},
    email: {type: String, unique: true, required:true},
    password: { type: String, required: true },
    picId: {type: Schema.Types.ObjectId},
    isVerified: { type: Boolean },
    verifyToken: { type: String },
    verifyExpires: { type: Date },
    verifyChanges: { type: Object },
    resetToken: { type: String },
    resetExpires: { type: Date }
  
  }, {
    timestamps: true
  });

  return mongooseClient.model('users', users);
};

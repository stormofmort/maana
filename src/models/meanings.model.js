// meanings-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const meanings = new Schema({
    text: { type: String, required: true },
    useId: {type:Schema.Types.ObjectId},
    wordId: {type:Schema.Types.ObjectId},
    userId: {type: Schema.Types.ObjectId, required: true },
    voteId: [{type: Schema.Types.ObjectId }],
    picId: {type: Schema.Types.ObjectId },
    commentId: [{type: Schema.Types.ObjectId}],
  }, {
    timestamps: true
  });

  return mongooseClient.model('meanings', meanings);
};

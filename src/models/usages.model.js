// usages-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const tenses = new Schema({
    _id: {type: Schema.Types.ObjectId},
    tensetype: {type:String}
  })
  const usages = new Schema({
    usetype: { type: String, required: true },
    word: {type:Schema.Types.ObjectId, required: true},
    tenses: [tenses],
    meanings: [{type:Schema.Types.ObjectId}],
    examples: [{type:Schema.Types.ObjectId}],
    translations: [{type:Schema.Types.ObjectId}],
    dialects: [{type:Schema.Types.ObjectId}],
    userId: {type: Schema.Types.ObjectId, required: true },
    voteId: [{type: Schema.Types.ObjectId}],
    picId: {type: Schema.Types.ObjectId},
    commentId: [{type: Schema.Types.ObjectId}],
  }, {
    timestamps: true
  });

  return mongooseClient.model('usages', usages);
};

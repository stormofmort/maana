// views-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const views = new Schema({
    viewdate: { type: Date, required: true },
    wordId: {type: Schema.Types.ObjectId, required: true}
  }, {
    timestamps: true
  });

  return mongooseClient.model('views', views);
};

const agenda = require('./jobs/agenda.js')

module.exports = function(app) {

  const jobTypes = process.env.JOB_TYPES ? process.env.JOB_TYPES.split(',') : [];

  jobTypes.forEach(type => {
    require('./jobs/' + type)(app);
  });

  if (jobTypes.length) {
    agenda.start(); // Returns a promise, which should be handled appropriately
  }

}

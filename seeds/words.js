const app = require('../src/app');
const _ = require('lodash');

module.exports = ()=>{
  const users =  app.service('users').find();
  return users.then((users)=>{
    console.log(users)
    const user = _.sample(users.data);
    return {
      path: 'words',
      count: 1,
      params: { 
        user
      },
      template: {
        // name: '{{name.firstName}} {{name.lastName}}'
        text: 'ފެސް'
      }
    };
  });
  
};

import { mapActions, mapGetters  } from 'vuex'
import {mapFields, Validator} from 'vee-validate'

export default {
  methods: {
    ...mapActions('users', ['create','find']),
  },
  mounted() {
    const isUnique = (value, args) => {
      var obj = {}
      if(args[0] == "email"){
        obj = {email: value}
      }else {
        obj = {username: value}
      }
      this.loading = true;
      return this.find({query:obj}).then((user)=>{
        this.loading = false;
        if(user.data.length > 0) {
          return false
        }
        return true
      }).catch((err)=>{
        this.loading = false;
        return false
      })
    }
    Validator.extend('unique', {
      
      validate: isUnique,
      getMessage: (field, params, data) => 'Must be unique'
    })
    
  }
}

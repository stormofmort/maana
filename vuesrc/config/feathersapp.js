const feathers = require('@feathersjs/client');
const io = require('socket.io-client');
const socket = io();
const auth = require('@feathersjs/authentication-client');

const app = feathers();

app.configure(feathers.socketio(socket));
app.configure(auth({storage: window.localStorage}))

module.exports = app;

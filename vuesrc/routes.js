import VueRouter from 'vue-router';

const App = () => import('components/index/App.vue');
const Home = () => import('components/home/Home.vue');
const Drawer = () => import('components/index/Drawer.vue');
const Toolbar = () => import('components/index/Toolbar.vue');
const Footer = () => import('components/index/Footer.vue');

const Word = () => import('components/word/Word.vue');
const Words = () => import('components/word/Words.vue');
const WordCreate = () => import('components/word/WordCreate.vue');
// const WordList = () => import('components/word/WordList.vue');
// const WordCard = () => import('components/word/WordCard.vue');

const Settings = () => import('components/setting/Settings.vue');

const Register =() => import('components/account/Register.vue');
const Profile =() => import('components/account/Profile.vue');
const Login =() => import('components/account/Login.vue');
const Verify =() => import('components/account/Verify.vue');
const Reset =() => import( 'components/account/Reset.vue');
const VerifyReset =() => import( 'components/account/VerifyReset.vue')

const NotFoundComponent = () => import('components/Notfound.vue');

const routes = [
  { 
    path: '/', 
    components: {
      default: App,
      drawer: Drawer,
      toolbar: Toolbar,
      footer: Footer
    },
    children: [{
      path: '',
      component: Home,
      name: 'home'
    },{
      path:'words',
      component: Words,
      name: 'words'
    },{
      path:'words/create',
      component: WordCreate,
      name: 'createword'
    },{
      path: 'word/:id',
      component: Word,
      name: 'word',
      props: true
    },{
      path: 'login',
      name: 'login',
      component: Login,
    },{
      path:'settings',
      component: Settings,
      name: 'settings'
    },{
      path:'verify/:token',
      name: 'verify',
      component: Verify,
      props: true
    },{
      path: 'register',
      name: 'register',
      component: Register
    },{
      path: 'profile',
      name: 'profile',
      component: Profile
    },{
      path: 'reset',
      name: 'reset',
      component: Reset
    },{
      path: 'verifyreset/:token',
      name: 'verifyreset',
      component: VerifyReset,
      props: true
    },{
      path: '*',
      name: '404notfound',
      component: NotFoundComponent
    }]
  }
];

const router = new VueRouter({
  mode: 'history',
  routes
});

export default router;

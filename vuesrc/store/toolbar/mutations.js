export default {
  changeExtComponent(state, value) {
    state.currentExtComponent =  value
  },
  changeTab(state, value) {
    state.tabs = value
  },
  changeCurrentTab(state, value) {
    state.currentTab = value
  }
}

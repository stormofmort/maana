export default {
  setExtComponent({commit}, value) {
    commit('changeExtComponent', value)
  },
  setTabProperty({commit}, value) {
    commit('changeTab', value)
  },
  setCurrentTab({commit}, value) {
    commit('changeCurrentTab', value)
  }
}

import Vue from 'vue'
import Vuex from 'vuex'
import feathersVuex from 'feathers-vuex'
import feathersClient from '../config/feathersapp'
import drawer from './drawer'
import toolbar from './toolbar'
import app from './app'

const { service, FeathersVuex, auth } = feathersVuex(feathersClient, { idField: '_id' })

Vue.use(Vuex)
Vue.use(FeathersVuex)

export default new Vuex.Store({
  modules: {
    app,
    drawer,
    toolbar
  },
  plugins: [
    service('words'),
    service('users'),
    auth({
      userService: 'users'
    })

    // Setup the auth plugin.
    // auth({ userService: 'users' })
  ]
})

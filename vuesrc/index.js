import Vue from 'vue';
import Vuetify from 'vuetify/lib';

import VeeValidate from 'vee-validate';
import VueRouter from 'vue-router';
import Meta from 'vue-meta';

import router from 'routes';
import store from 'store';
import root from 'components/index/Index.vue';

//require('vuetify/dist/vuetify.min.css');
require('material-design-icons-iconfont/dist/material-design-icons.css');
import 'stylus/main.styl';

Vue.use(Vuetify);
Vue.use(VueRouter);
Vue.use(Meta);
Vue.use(VeeValidate);

// const router = new VueRouter({routes});

const app = new Vue({
  el: '#app',
  router,
  store,
  render: createElement => createElement(root)
});

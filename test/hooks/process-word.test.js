const assert = require('assert');
const feathers = require('@feathersjs/feathers');
const processWord = require('../../src/hooks/process-word');

describe('\'process-word\' hook', () => {
  let app;

  beforeEach(() => {
    app = feathers();

    app.use('/dummy', {
      async get(id) {
        return { id };
      }
    });

    app.service('dummy').hooks({
      before: {
        create: processWord(),
      }
    });
  });

  it('runs the hook', async () => {
    const result = await app.service('dummy').get('test');
    
    assert.deepEqual(result, { id: 'test' });
  });
});

const assert = require('assert');
const app = require('../../src/app');

describe('\'dialects\' service', () => {
  it('registered the service', () => {
    const service = app.service('dialects');

    assert.ok(service, 'Registered the service');
  });
});

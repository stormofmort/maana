const assert = require('assert');
const app = require('../../src/app');

describe('\'examples\' service', () => {
  it('registered the service', () => {
    const service = app.service('examples');

    assert.ok(service, 'Registered the service');
  });
});

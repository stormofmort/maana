const assert = require('assert');
const app = require('../../src/app');
const validator = require('validator');
const faker = require('faker');
const should = require('chai').should();

const {onlydhivehi, letterwithfili, firstlettersukun, wronglettersukun, sukunexists} = require('../../src/validations/conformity');

describe('\'words\' service', () => {
  it('registered the service', () => {
    const service = app.service('words');
    assert.ok(service, 'Registered the service');
  });
  it('checks for dhivehi only', ()=>{
    var result = validator.matches(faker.lorem.word(), onlydhivehi.arguments);
    result.should.equal(false, 'value is false');
  });
  it('checks for correct sukun', ()=>{
    var word = "ހުސް";
    var result = validator.matches(word, wronglettersukun.arguments);
    result.should.equal(true, 'value is true');
    word = "ހުދް"
    result = validator.matches(word, wronglettersukun.arguments);
    result.should.equal(false, 'value is false');
    word = "ސުދު"
    result = validator.matches(word, wronglettersukun.arguments);
    result.should.equal(true, 'value is true');
    word = "ންދު";
    result = validator.matches(word, firstlettersukun.arguments);
    result.should.equal(false, 'value is false');
  });
  it('check for letter with fili', ()=>{
    var word = 'މީމމީ';
    var result = validator.matches(word, letterwithfili.arguments);
    result.should.equal(false, 'value is false');
    word = 'މީމީދީ';
    result = validator.matches(word,letterwithfili.arguments);
    result.should.equal(true, 'value is true')
  });
});

const assert = require('assert');
const app = require('../../src/app');

describe('\'usages\' service', () => {
  it('registered the service', () => {
    const service = app.service('usages');

    assert.ok(service, 'Registered the service');
  });
});

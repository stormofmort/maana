const assert = require('assert');
const app = require('../../src/app');

describe('\'meanings\' service', () => {
  it('registered the service', () => {
    const service = app.service('meanings');

    assert.ok(service, 'Registered the service');
  });
});

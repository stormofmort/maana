const assert = require('assert');
const app = require('../../src/app');

describe('\'tenses\' service', () => {
  it('registered the service', () => {
    const service = app.service('tenses');

    assert.ok(service, 'Registered the service');
  });
});
